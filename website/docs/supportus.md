---
id: supportus
title: Support us
sidebar_label: Support us
---

The easiest way to help <i>ChannelAttribution</i> is fill in this  <a href="http://app.channelattribution.net/form/">Survey</a>.

You could also consider to become our Sponsor or make a Donation. <a href="mailto:info@channelattribution.net"> Contact us</a>.

