import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <><center><a href="https://www.getyourguide.com">Get Your Guide</a></center></>,
    imageUrl: 'img/getyourguide.jpeg',
  },
  {
    title: <><center><a href="https://www.holidaycheck.de">HolidayCheck</a></center></>,
    imageUrl: 'img/holidaycheck.png',
  },
  {
    title: <><center><a href="https://www.impactextend.dk">Impact Extend</a></center></>,
    imageUrl: 'img/impactextend.png',
  },  
  {
    title: <><center><a href="https://www.albelli.nl">Albelli</a></center></>,
    imageUrl: 'img/albelli.jpeg',
  },
  {
    title: <><center><a href="https://www.dotpe.in">Dotpe</a></center></>,
    imageUrl: 'img/dotpe.png',
  }, 
  {
    title: <><center><a href="https://www.data4sales.com">Data4Sales</a></center></>,
    imageUrl: 'img/data4sales.jpeg',
  },  
  {
    title: <><center><a href="https://www.holidu.com">Holidu</a></center></>,
    imageUrl: 'img/holidu.png',
  }, 
  {
    title: <><center><a href="https://www.bergzeit.de">Bergzeit</a></center></>,
    imageUrl: 'img/bergzeit.png',
  },    

  
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <main>
	   <br></br>
	   <h2><center>Do you use ChannelAttribution? Let us know it, please! Fill in this one minute <a href="http://app.channelattribution.net/form/">Survey</a>. </center></h2>
	   <br></br>
	   <h3><b><em><center>The following companies are using ChannelAttribution:</center></em></b></h3>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
